package com.oreillyauto.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.JsonObject;

public class Helper {

	private final static String GOOGLE_URL_SHORTENER_API_KEY = "AIzaSyCkhv6zWBUnQu0UEcrvbyUmZNkeWWywio8";
	
    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    
    public static void main(String[] args) throws Exception {
    	
    	// https://firebase.google.com/docs/dynamic-links/rest
    	// Web API Key: AIzaSyBBpgVfeCJ-DQpiz-AtII29DipKIilxVjA
    	String longUrl = "https://firebase.google.com/docs/reference/dynamic-links/link-shortener";
    	
    	String url = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyBBpgVfeCJ-DQpiz-AtII29DipKIilxVjA";
    	
    	JsonObject json = new JsonObject();
    	json.addProperty("longDynamicLink", longUrl);
    	JsonObject suffix = new JsonObject();
    	suffix.addProperty("option", "SHORT");
    	json.add("suffix", suffix);
    	
    	System.out.println(json);
    	
		URL myurl = new URL(url);
		HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);

		con.setRequestProperty("Content-Type", "application/json;");
		con.setRequestProperty("Accept", "application/json,text/plain");
		con.setRequestProperty("Method", "POST");
		OutputStream os = con.getOutputStream();
		os.write(json.toString().getBytes("UTF-8"));
		os.close();

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();
		
		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			
			br.close();
			System.out.println("" + sb.toString());
		} else {
			System.out.println(con.getResponseCode());
			System.out.println(con.getResponseMessage());
		}
 	
    	
    	
/*    	RestTemplate restTemplate = new RestTemplate();
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	//headers.set("Content-Type", "application/json");
    	//headers.setContentType(new MediaType("application","json"));
    	
    	JsonObject json = new JsonObject();
    	json.addProperty("longDynamicLink", longUrl);
    	JsonObject suffix = new JsonObject();
    	suffix.addProperty("option", "SHORT");
    	json.add("suffix", suffix);
    	
    	HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);
    	//ResponseEntity<String> response = restTemplate.put(url, entity);
    	
    	ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    	System.out.println("response=" + response);*/
    		
    	
    	
    	
    	
    	
    	
    	
    	
    	// JavaScript Example
/*    	<script src="https://www.gstatic.com/firebasejs/5.7.3/firebase.js"></script>
    	<script>
    	  // Initialize Firebase
    	  var config = {
    	    apiKey: "AIzaSyBBpgVfeCJ-DQpiz-AtII29DipKIilxVjA",
    	    authDomain: "internproject-bcbc2.firebaseapp.com",
    	    databaseURL: "https://internproject-bcbc2.firebaseio.com",
    	    projectId: "internproject-bcbc2",
    	    storageBucket: "internproject-bcbc2.appspot.com",
    	    messagingSenderId: "319360972774"
    	  };
    	  firebase.initializeApp(config);
    	</script>*/
    	
    	
    	
    	
    	
    	// THIS API IS NO LONGER BEING SUPPORTED   :-(
    	// https://developers.google.com/url-shortener/v1/getting_started
    	
    		
/*        // Get an instance of the rest template and define the URL
        RestTemplate restTemplate = new RestTemplate();
        String URL = "https://api.twilio.com/2010-04-01/Accounts/"+TWILIO_ACCOUNT_SID+"/Messages.json";
        
        // Build the headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
        
        // OPTION 1 - REQUEST VIA EXCHANGE USING "POST", "MULTIVALUEMAP", AND "HTTPHEADERS"
        //HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        
        // Build the MultiValueMap that contains the form key and value pairs
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("Body", "hi");
        map.add("From", "+14173175204");
        map.add("To", "+14174239219");
        map.add(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
        
        // Create an HttpEntity object with the map and the headers
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        
        // Make the web service call
        ResponseEntity<String> response = restTemplate.exchange(URL, HttpMethod.POST, request, String.class);
        
        // Print the response
        System.out.println("response=" + response);
        
        // - OR -
        
        // OPTION 2 - REQUEST VIA POSTFOROBJECT() USING "ENTITY"
        // HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        // restTemplate.postForObject(url, entity, String.class); 
        
        
        //ResponseEntity<String> response = restTemplate.postForEntity("url", null, String.class);
        //HttpStatus status = response.getStatusCode();
        //String restCall = response.getBody();   
        
        return result;*/
    }
    
}
